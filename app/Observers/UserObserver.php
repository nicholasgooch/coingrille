<?php
namespace App\Observers;

use App\User;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(User $user)
    {
        
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        $user->userData()->delete();
        $user->deposits()->delete();
        $user->withdrawals()->delete();
        $user->transactions()->delete();
        $user->cards()->delete();
    }
}